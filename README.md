# teaching_platform

#### 介绍
信息化在线教学平台网站

#### 软件架构
信息化在线教学平台的软件架构致力于提供稳定、高效、安全的在线学习体验。它采用分层架构，包括用户界面层、业务逻辑层、数据访问层和基础设施层，以确保系统的灵活性和可扩展性。平台支持课程管理、用户管理、订单管理、支付管理和数据统计等核心功能，满足在线教学的多样化需求。技术实现上，平台采用前后端分离的开发模式提高开发效率和可维护性，利用微服务架构实现服务的解耦和灵活扩展，运用缓存技术加速数据访问速度，通过分布式部署提高系统可用性和容灾能力，并采用HTTPS协议和数据加密技术确保数据安全。

####开发环境
操作系统 ： windows

Java环境 ： JDK1.8(不能使用高版本)

开发工具 ： Idea 2021
数据库： mysql 5.5以上

spring boot : 2.1.7 Release

####测试方法
推荐使用：谷歌浏览器
后台地址
http://localhost:8080/springboot9qw88/admin/dist/index.html

管理员  caiqijia 密码 123


前台地址：http://localhost:8080/springboot9qw88/front/index.html
 
在src\main\resources\application.yml中编辑
											
	 url: jdbc:mysql://127.0.0.1:3306/springboot9qw88?useUnicode=true&characterEncoding=utf-8&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC
        username: root
        password: 1234 （数据库密码）（注意修改密码）

![输入图片说明](https://foruda.gitee.com/images/1718342109097666184/93e76151_14096877.png "屏幕截图")